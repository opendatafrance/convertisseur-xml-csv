@extends('layout')

@section('head')
    <script src="js/monJs.js"></script>
@endsection
@section('contenu')


    <!--<form method="post" action="{{route('traitement')}}" enctype="multipart/form-data">
            <div class="align-self-center">
                <div class="col align-self-center">
                    <div class="form-group">
                        <label for="fichier" style="color:#868383"> Selectionner un fichier ...</label></br>
                        <input type="file" class="" id="fichier" name="fichier">
                    </div>
                </div>

                <input type="hidden" value="{{csrf_token()}}" name="_token">
                <input type="submit" class="btn btn-primary" value="Valider et Convertir">
            </div>
        </form>-->
        @if(Session::has('ok'))
            <div class="row">
                <div class="col align-self-center">
                    <div class="alert alert-success" role="alert">
                        {{Session::get('ok')}}
                        <br>
                        <a href={{url('xmlTOCsvFichier.csv')}}>Télécharger le Csv </a><!-- c'est pas la bonne methode-->
                    <!-- <a href={{url('xmlToJsonFichier.json')}}>Télécharger le Json </a>-->
                    </div>
                </div>
            </div>
        @endif
        @if(Session::has('erreur'))
            <div class="row">
                <div class="col align-self-center">
                    <div class="alert alert-danger" role="alert">
                        {{Session::get('erreur')}}
                    </div>
                </div>
            </div>
        @endif

    <div class="row justify-content-md-center">
        <div class="">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                Choisissez un fichier Xml
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            <form method="post" action="{{route('traitement')}}" enctype="multipart/form-data">
                                <div class="align-self-center">
                                    <div class="col align-self-center">
                                        <div class="form-group">
                                            <label for="fichier" style="color:#868383"> Selectionner un fichier
                                                ...</label></br>
                                            <input type="file" class="" id="fichier" name="fichier" required>
                                        </div>
                                    </div>

                                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                                    <input type="submit" class="btn btn-primary" value="Valider et Convertir">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                    data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Indiquez l'URL d'un fichier Xml
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <form method="post" action="{{route('traitement')}}" enctype="multipart/form-data">
                                <div class="align-self-center">
                                    <div class="col align-self-center">
                                        <div class="form-group">
                                            <label for="fichier" style="color:#868383"> Url :</label></br>
                                            <input type="text" class="form-control" id="url" name="url"
                                                   placeholder="https://www." required>
                                        </div>
                                    </div>
                                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                                    <input type="submit" class="btn btn-primary" value="Valider et Convertir">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection