<?php

namespace convertisseurXML;

use Illuminate\Database\Eloquent\Model;

class Erreurslog extends Model
{
    //
    protected $fillable = ["id","nbrMarches","nbrColonneDesordre","nbrChampsManquants","nbrLigne"];
}
