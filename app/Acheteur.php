<?php

namespace convertisseurXML;

use Illuminate\Database\Eloquent\Model;

class Acheteur extends Model
{
    //
    protected $fillable = ["id","nom","idMarche"];
}
