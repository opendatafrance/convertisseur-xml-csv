<?php

namespace convertisseurXML;

use Illuminate\Database\Eloquent\Model;

class Champssurnumeraire extends Model
{
    //
    protected $fillable = ["id","cleChamp","valeurChamp","idMarche"];
}
