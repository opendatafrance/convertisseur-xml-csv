<?php

namespace convertisseurXML\Http\Middleware;

use Closure;

class VerificationParam
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param = $request->all();
        if(count($param)!=2)
        {
            return redirect('validateur');
        }
        return $next($request);
    }
}
