<?php


namespace convertisseurXML\Http\Outils;

trait ConfCsv {

    public function existeFichier()
    {

        if(file_exists("Conf.csv")){
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * @return array le fichier de paramettrage
     */
    public function getListesParam()
    {
        $listes = array();
        if (($handle = fopen("Conf.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $liste = array();
                for ($i = 0; $i < $num; $i++) {
                    $liste[$i] = $data[$i];
                }
                array_push($listes,$liste);
            }
            fclose($handle);
        }
        return $listes;
    }

    /**
     * @param $liste la liste
     * @return int
     */
    public function nombreElementsListe($liste)
    {
        return count($liste);
    }

    /**
     * @return array|null les nom XML qui existe dans le fichier de paramettrage sinon retourn null
     */
    public function getlisteNomXml(){
        try{
            $listes = $this->getListesParam();
            $champsXml = array();
            foreach ($listes as $liste)
            {
                $champsXml[]=$liste[0];
            }
            return $champsXml;
        }
        catch(\Exception $e)
        {
            return null;
        }

    }

    /**
     * @return array|null les nom Csv qui existe dans le fichier de paramettrage sinon retourn null
     */
    public function getlisteEnteteCsv(){
        try{
            if (($handle = fopen("entete.csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    for ($i = 0; $i < $num; $i++) {
                        $liste[] = $data[$i];
                    }
                }
                fclose($handle);
            }
            return $liste;
        }
        catch(\Exception $e)
        {
            return null;
        }

    }

    /**
     * @return array|null retourn la liste des valeurs du champ obligatoir ou pas
     */
    public function getlisteObligatoirNonObligatoir(){
        try{
            $listes = $this->getListesParam();
            $oubligatoirOuPas = array();
            foreach ($listes as $liste)
            {
                $oubligatoirOuPas[]=$liste[2];
            }
            return $oubligatoirOuPas;
        }
        catch (\Exception $e)
        {
            return null;
        }
    }

    /**
     * @param $valeur champ XML
     * @return |null 1 si la valeur est obligatoir , 0 si la valeur est optionnelle et null si le champ n'existe pas
     */
    public function obligatoirOuPas($valeur){
        try
        {
            $listeNomsXml = $this->getlisteNomXml();
            $listeObligatoir = $this->getlisteObligatoirNonObligatoir();
            $cle = array_search($valeur,$listeNomsXml);
            return $listeObligatoir[$cle];
        }
        catch(\Exception $e)
        {
            return null;
        }
    }

    /**
     * @param $valeur c'est le champs xml du l'element courant
     * @return |null 1 si le champ est obligatoir et 0 si le champ est facultatif
     */
    public function modificationTitulaireFacultatifObligatoir($valeur)
    {
        $listeFacObliReverse = array();
        $listenomXmlRenverse = array();
        try{
            $listeFacObliReverse =  array_reverse($this->getlisteObligatoirNonObligatoir());
            $listenomXmlRenverse = array_reverse($this->getlisteNomXml());
            $cle=array_search($valeur,$listenomXmlRenverse);
            return $listeFacObliReverse[$cle];
        }
        catch (\Exception $e)
        {
            return null;
        }
    }


    public function nombreChampsObligatoir()
    {
        $champs = $this->getlisteObligatoirNonObligatoir();
        $cpt = 0;
        for ($i=0;$i<count($champs);$i++)
        {
            if($champs[$i] == 1)
            {
                $cpt++;
            }
        }
        return $cpt;
    }

}

class configurationCSV
{
    use ConfCsv;
}