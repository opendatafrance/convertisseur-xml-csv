<?php


namespace convertisseurXML\Http\Outils;

use convertisseurXML\Acheteur;
use convertisseurXML\Champssurnumeraire;
use convertisseurXML\Erreurslog;
use convertisseurXML\Modification;
use convertisseurXML\Titulaire;
use convertisseurXML\Marche;
use Illuminate\Support\Facades\DB;

trait traitementBaseDonneese{

    /**
     * fonction permet d'inserer l'id du marche
     * @param $idMarche l'insertion de l'id du marche
     * @return id marche
     */
    public function insererIdMarche($idMarche){
        $marche = new Marche();
        $marche->idMarche = $idMarche;
        $marche->save();
        return $marche->id;
    }

    /**
     * fonction permet d'inserer un acheteur
     * @param $id acheteur
     * @param $nom acheteur
     * @param $idMarche reference du marche
     * @return boolean true c'est l'insertion est faite sinon false
     */
    public function insererAcheteur($id,$nom,$idMarche)
    {
        try{
            $acheteur = new Acheteur();
            $acheteur->id= $id;
            $acheteur->nom= $nom;
            $acheteur->idMarche= $idMarche;
            $acheteur->save();
            if($acheteur->id != null) {
                return true;
            }
            else {
                return false;
            }
        }
        catch(\Exception $e)
        {
            return false;
        }

    }

    /**
     * @param $MARCHE_ID
     * @param $MARCHE_UID
     * @param $NATURE_MARCHE
     * @param $MARCHE_OBJET
     * @param $CPV_CODE
     * @param $PROCEDURE
     * @param $LIEU_EXEC_CODE
     * @param $LIEU_EXEC_TYPE
     * @param $LIEU_EXEC_NOM
     * @param $DUREE_MOIS
     * @param $NOTIFICATION_DATE
     * @param $PUBLICATION_DATE
     * @param $MONTANT
     * @param $PRIX_FORME
     * @return True si la modification est faite sinon retourne False
     */
    public function modifierMarche($MARCHE_ID,$MARCHE_UID,$NATURE_MARCHE,$MARCHE_OBJET,$CPV_CODE,$PROCEDURE,$LIEU_EXEC_CODE,$LIEU_EXEC_TYPE,$LIEU_EXEC_NOM,$DUREE_MOIS,$NOTIFICATION_DATE,$PUBLICATION_DATE,$MONTANT,$PRIX_FORME)
    {
        try{
            MARCHE::find($MARCHE_ID)
                ->update(['iud'=>$MARCHE_UID,'nature'=>$NATURE_MARCHE,'objet'=>$MARCHE_OBJET,'codeCPV'=>$CPV_CODE,
                    'procedure'=>$PROCEDURE,'code'=>$LIEU_EXEC_CODE,'typeCode'=>$LIEU_EXEC_TYPE,
                    'nom'=>$LIEU_EXEC_NOM,'dureeMois'=>$DUREE_MOIS,'dateNotification'=>$NOTIFICATION_DATE,
                    'datePublicationDonnee'=>$PUBLICATION_DATE,'montant'=>$MONTANT,'formePrix'=>$PRIX_FORME]);

        }
        catch(\Exception $e)
        {
            return false;
        }

    }

    /**
     * Permet l'insertion d'un titulaire
     * @param $tit liste contien les valuer id, type id et denomination sociale
     * @param $MARCHE_ID la reference du marche
     */
    public function insrerTitulaire($typeIdentifiant,$idTitulaire,$denominationS,$MARCHE_ID)
    {
        try{
            $titulaire = new Titulaire();
            $titulaire->typeIdentifiant= $typeIdentifiant;
            $titulaire->id= $idTitulaire;
            $titulaire->denominationSociale= $denominationS;
            $titulaire->idMarche= $MARCHE_ID;
            $titulaire->idModification= " ";
            $titulaire->save();

            if($titulaire->id != null) {
                return true;
            }
            else {
                return false;
            }
        }
        catch(\Exception $e)
        {
            return felse;
        }

    }

    /**
     * Permet l'insertion d'une Modification
     *
     * @param $MODIF_OBJET l'objet de la modification
     * @param $MODIF_PUBLICATION_DATE la date de la modification
     * @param $MODIF_NOTIFICATION_DATE la date de la notification
     * @param $DUREE_MOIS la duree en moi
     * @param $MONTANT le montant
     * @param $MARCHE_ID la reference du marche
     * @return l'id de la modification
     */
    public function insererModification($MODIF_OBJET,$MODIF_PUBLICATION_DATE,$MODIF_NOTIFICATION_DATE,$DUREE_MOIS,$MONTANT,$MARCHE_ID)
    {
        try{
            $modification = new Modification();
            $modification->objetModification = $MODIF_OBJET;
            $modification->datePubDonneesModification = $MODIF_PUBLICATION_DATE;
            $modification->dateNotifModification = $MODIF_NOTIFICATION_DATE;
            $modification->dureeMois = $DUREE_MOIS;
            $modification->montant = $MONTANT;
            $modification->idMarcheModification = $MARCHE_ID;
            $modification->save();
            return $modification->id;
        }
        catch(\Exception $e)
        {
            return null;
        }

    }

    /**
     * Permet l'insertion d'un titulaire d'une modification
     * @param $t
     * @param $MARCHE_ID
     * @param $idModification
     * @return true si l'insertion est effecuer sinon renvoi false
     */
    public function insererTitulaireModification($typeIdentifiant,$idTitulaire,$denominationS,$idModification)
    {
        try{
            $titulaireMod = new Titulaire();
            $titulaireMod->typeIdentifiant= $typeIdentifiant;
            $titulaireMod->id= $idTitulaire;
            $titulaireMod->denominationSociale= $denominationS;
            $titulaireMod->idMarche= " ";
            $titulaireMod->idModification = $idModification;
            $titulaireMod->save();
            if($titulaireMod->id)
            {
                return true;
            }
            else {
                return false;
            }
        }
        catch(\Exception $e)
        {
            return false;
        }
    }

    public function nbrChampsSurnumeraire()
    {
        try{
            //$nombre = DB::select("SELECT count(*) as 'nbr' FROM champssurnumeraires ");
            $count = Champssurnumeraire::count();
            return $count;
        }
        catch (\Exception $e)
        {
            return null;
        }
    }

    public function getLog()
    {
        try{
            $log = Erreurslog::all();
            return $log;
        }
        catch(\Exception $e)
        {
            return null;
        }


    }

    /**
     * @return array la liste des marches a affiché
     */
    public function recupererData()
    {
        $listes = array();
        $liste = DB::select("select marches.idMarche as 'idMarche' ,  marches.iud , acheteurs.id as idAcheteur , acheteurs.nom as nomAcheteur , marches.nature
                , marches.objet , marches.codeCPV , marches.procedure , marches.code 
                , marches.typeCode , marches.nom as 'nomMarche' , marches.dureeMois as 'dureeMoisMarche' , marches.dateNotification ,
                 marches.datePublicationDonnee , marches.montant as 'montantMarche' , marches.formePrix ,
                 tit.id as 'idTitulaire' , tit.typeIdentifiant , tit.denominationSociale ,
                 modifications.objetModification , modifications.dateNotifModification ,
                 modifications.datePubDonneesModification , modifications.dureeMois as 'dureeMoisModif' ,
                 modifications.montant as MontantModif ,
                 titMod.id as 'idTitulaireModification' , titMod.typeIdentifiant as 'typeIdentiModification' , titMod.denominationSociale as 'dominationSocialeModification' , (Champssurnumeraires.cleChamp  ||' : '||Champssurnumeraires.valeurChamp) AS CHAMPS_SURNUMERAIRE
                from marches left join acheteurs on marches.id = acheteurs.idMarche
                left join titulaires as  'tit'  on tit.idMarche = marches.id
                left join modifications on marches.id=modifications.idMarcheModification
                left join titulaires as 'titMod' on titMod.idModification = modifications.id
                left join Champssurnumeraires on Champssurnumeraires.idMarche = marches.id");
        foreach ($liste as $l)
        {
            $li = (array) $l;
            array_push($listes,$li);
        }
        return $listes;
    }

    /**
     * fonction permet de vider les tables de la base
     * @return true si le contenu des tables est supprimé.
     */
    public function ViderLesTables()
    {
        try{
            Marche::query()->truncate();
            Acheteur::query()->truncate();
            Titulaire::query()->truncate();
            Modification::query()->truncate();
            Champssurnumeraire::query()->truncate();
            Erreurslog::query()->truncate();
            return true;
        }
        catch(\Exception $e)
        {
            return false;
        }
    }

    /**
     * @param $cle du chmaps surnumeraire
     * @param $valeur du champs surnumeraire
     * @param $idMarche l'id du marche
     * @return true si l'insertion est effectué
     */
    public function insererChampsFacultatif($cle,$valeur,$idMarche)
    {
        try{
            $champs = new Champssurnumeraire();
            $champs->cleChamp=$cle;
            $champs->valeurChamp=$valeur;
            $champs->idMarche=$idMarche;
            $champs->save();
            if($champs->id != null) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (\Exception $e)
        {
         return false;
        }

    }

    /**
     * @param $nbrmarches nombre des marche parcouru
     * @param $champsmanquent nombre de champs qui manque
     * @param $champsParcouru champs  parcouru
     * @return bool retturn true si l'insertion est effectuer sinon elle renvoi false
     */
    public function insererlogErreur($nbrmarches,$champsmanquent,$champsParcouru){
        try{
            $erreurs = new Erreurslog();
            $erreurs->nbrMarches = $nbrmarches;
            $erreurs->nbrColonneDesordre = 0;
            $erreurs->nbrChampsManquants = $champsmanquent;
            $erreurs->nbrLigne=$champsParcouru;
            $erreurs->save();
            if($erreurs->id != null) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (\Exception $e)
        {
            return false;
        }

    }
}

class ServiceBD
{
use traitementBaseDonneese;
}