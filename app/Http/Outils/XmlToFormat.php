<?php


namespace convertisseurXML\Http\Outils;

use convertisseurXML\Http\Outils\configurationCSV;
use convertisseurXML\Http\Outils\ServiceBD;

trait xmlToCsv
{

    static $maliste = array();

    /**
     * @param $fichier le fichier envoger par l'utilisateur
     * @return  true si la verification d'ordre est faire et l'insertion dans la base de Données
     */
    public function verificationChamps($fichier)
    {

        try {
            //recuperation des paramettres de config
            $conf = new configurationCSV();
            $service = new ServiceBD();
            if($conf->existeFichier()){
                $champsConfXml = $conf->getlisteNomXml();
                $champsConfObliFac = $conf->getlisteObligatoirNonObligatoir();
                $listeDecoupe = array();
                $indice = 0;

                $etat = true;
                $data = array();
                $xml = new \DOMDocument();
                $xml = simplexml_load_file($fichier);
                $listeComplete = array();//liste contient les marche avec les detailles sur chaque champs
                //dd( $xml->children());
                foreach ($xml->children() as $of) {
                    $listeComplete = $this->recparcour($of, "null", $indice);
                    $indice++;
                }
                //dd($listeComplete);
                $nbrchampsTotal = count($listeComplete);
                $listeDecoupe = $this->decouperListeMarches($listeComplete);
                $listeDecoupe = $this->affectationIndice($listeDecoupe, $champsConfXml);
                //dd($listeDecoupe);
                $etat = $this->verificationOrdre($listeDecoupe,$conf,$service);
                //dd($etat);
                if ($etat) {
                    $this->implementationBD($listeDecoupe, $champsConfXml, $nbrchampsTotal,$conf,$service);
                    $data = $service->recupererData();//peut etre que je cree deux fonction
                    $entet = $conf->getlisteEnteteCsv();
                    $nbrsurnumeraire = $service->nbrChampsSurnumeraire();
                    if($nbrsurnumeraire==0)
                    {
                        array_unshift($data,$entet);
                        $this->creerFichierCsv($data);
                    }
                    else{

                        $entet[]="CHAMP_SURNUMERAIRE";
                        array_unshift($data,$entet);
                        $this->creerFichierCsv($data);
                    }

                     return $service->getLog();
                } else {
                    return null;
                }
            }

        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $listeAverifier le contenu du fichier xml
     * @param $champsxml les nom xml du fichier de configuration
     * @param $champsobligatoirFacultatif la liste des 0 et 1 pour pour savoir est ce qu'un champs est obligatoir
     * @return true si les champs sont en ordre sinon return false
     */
    public function verificationOrdre($listeAverifier,$conf,$service)
    {
        try {
            $listeObli = $conf->getlisteObligatoirNonObligatoir();
            $listeXml = $conf->getlisteNomXml();
            $etat = true;

            $service->ViderLesTables();
            foreach ($listeAverifier as $liste) {
                $order = 0;
                $nbrObligatoir = $conf->nombreChampsObligatoir();
                $nbrElements = count($liste);
                if ($nbrObligatoir > $nbrElements) {
                    $etat = false;
                } else if ($nbrObligatoir < $nbrElements) {
                    $etat = $this->traitementCasImbrication($liste, $listeXml, $listeObli);
                } else {
                    $etat = $this->traitementCasNormal($liste, $listeXml, $listeObli);
                }

                if ($etat==false) {
                    return $etat;
                }
            }
            return $etat;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function implementationBD($listeAverifier, $champsxml, $nbrchmaps,$conf,$service)
    {
        $champs = " ";
        $valeur = " ";
        $parent = " ";
        $MARCHE_ID = " ";
        $MARCHE_UID = " ";
        $ACHETEURS_ID = " ";
        $ACHETEURS_NOM = " ";
        $NATURE_MARCHE = " ";
        $MARCHE_OBJET = " ";
        $CPV_CODE = " ";
        $PROCEDURE = " ";
        $LIEU_EXEC_CODE = " ";
        $LIEU_EXEC_TYPE = " ";
        $LIEU_EXEC_NOM = " ";
        $DUREE_MOIS = " ";
        $NOTIFICATION_DATE = " ";
        $PUBLICATION_DATE = " ";
        $MONTANT = " ";
        $PRIX_FORME = " ";
        $TITULAIRES_ID = " ";
        $TITULAIRES_ID_TYPE = " ";
        $TITULAIRES_DENOMINATION = " ";
        $MODIF_OBJET = " ";
        $MODIF_NOTIFICATION_DATE = " ";
        $MODIF_PUBLICATION_DATE = " ";
        $MODIF_DUREE_MOIS = " ";
        $MODIF_MARCHE_MONTANT = " ";
        $MODIF_TITULAIRES_ID = " ";
        $MODIF_TITULAIRES_ID_TYPE = " ";
        $MODIF_TITULAIRES_DENOMINATION = " ";
        $test = false;
        $estTitulaire = true;
        $idModif = null;
        $estMarche = true;
        $nbrchampsmanquent = 0;
        $nbrMarches=0;
        $listeObli = $conf->getlisteObligatoirNonObligatoir();
        $listeXml = $conf->getlisteNomXml();
        try {

            $nbrMarches=count($listeAverifier);
            $service->ViderLesTables();
            foreach ($listeAverifier as $liste) {
                $order = 0;
                //$nbrMarches++;
                foreach ($liste as $l) {
                    //la cle contien la concatenation du parent et du champs
                    $champxml = $l[0];//cle
                    $valeur = $l[1];//valeur
                    $parentKey = $l[2];//parent
                    $obligatoir = $conf->obligatoirOuPas($parentKey);
                    if (in_array($parentKey, $champsxml)) {

                        if (($parentKey == $listeXml[0])) {//"id"
                            if (isset($valeur) && ($valeur != "")) {
                                $id = $service->insererIdMarche($valeur);
                                $MARCHE_ID = $id;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MARCHE_ID = " ";
                            }
                        }
                        if (($parentKey == $listeXml[1])) {//"uid"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $MARCHE_UID = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MARCHE_UID = " ";
                            }
                        }

                        if ($parentKey ==$listeXml[2] ) {//"acheteur:id"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $ACHETEURS_ID = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $ACHETEURS_ID = " ";
                            }
                        }
                        if ($parentKey == $listeXml[3]) { //"acheteur:nom"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $ACHETEURS_NOM = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $ACHETEURS_NOM = " ";
                            }
                            $service->insererAcheteur($ACHETEURS_ID, $ACHETEURS_NOM, $MARCHE_ID);
                        }
                        if ($parentKey == $listeXml[4]) {//"nature"
                            if ((isset($valeur)) && (($valeur != ""))) {
                                $NATURE_MARCHE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $NATURE_MARCHE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[5]) {//"objet"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $MARCHE_OBJET = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MARCHE_OBJET = " ";
                            }
                        }
                        if ($parentKey == $listeXml[6]) {//"codeCPV"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $CPV_CODE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $CPV_CODE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[7]) {//"procedure"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $PROCEDURE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $PROCEDURE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[8]) {//"lieuExecution:code"
                            if ((isset($valeur) && ($valeur != ""))) {
                                $LIEU_EXEC_CODE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $LIEU_EXEC_CODE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[9]) { //"lieuExecution:typeCode"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $LIEU_EXEC_TYPE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $LIEU_EXEC_TYPE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[10]) { //"lieuExecution:nom"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $LIEU_EXEC_NOM = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $LIEU_EXEC_NOM = " ";
                            }
                        }
                        if ($parentKey == $listeXml[11]) {// "dureeMois"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $DUREE_MOIS = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $DUREE_MOIS = " ";
                            }
                        }
                        if (($parentKey == $listeXml[12]) && ($valeur != "")) {//"dateNotification"
                            if (isset($valeur)) {
                                $NOTIFICATION_DATE = substr($valeur,0,10);
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $NOTIFICATION_DATE = " ";
                            }
                        }
                        if (($parentKey == $listeXml[13]) && ($valeur != "")) { //"datePublicationDonnees"
                            if (isset($valeur)) {
                                $PUBLICATION_DATE = substr($valeur,0,10);
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $PUBLICATION_DATE = " ";
                            }
                        }
                        if (($parentKey == $listeXml[14]) && ($valeur != "")) { //"montant"
                            if (isset($valeur)) {
                                $MONTANT = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MONTANT = " ";
                            }
                        }
                        if (($parentKey == $listeXml[15]) && ($valeur != "")) { //"formePrix"
                            if (isset($valeur)) {
                                $PRIX_FORME = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $PRIX_FORME = " ";
                            }
                            $service->modifierMarche($id, $MARCHE_UID, $NATURE_MARCHE, $MARCHE_OBJET, $CPV_CODE, $PROCEDURE, $LIEU_EXEC_CODE, $LIEU_EXEC_TYPE, $LIEU_EXEC_NOM, $DUREE_MOIS, $NOTIFICATION_DATE, $PUBLICATION_DATE, $MONTANT, $PRIX_FORME);
                        }
                        if ($estTitulaire) {
                            if ($parentKey == $listeXml[16]) { //"titulaire:typeIdentifiant"
                                if ((isset($valeur)) && ($valeur != "")) {
                                    $TITULAIRES_ID_TYPE = $valeur;
                                } else {
                                    $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                    $TITULAIRES_ID_TYPE = " ";
                                }
                            }
                            if ($parentKey == $listeXml[17]) { //"titulaire:id"
                                if ((isset($valeur)) && ($valeur != "")) {
                                    $TITULAIRES_ID = $valeur;
                                } else {
                                    $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                    $TITULAIRES_ID = " ";
                                }
                            }
                            if ($parentKey == $listeXml[18]) { //"titulaire:denominationSociale"
                                if ((isset($valeur)) && ($valeur != "")) {
                                    $TITULAIRES_DENOMINATION = $valeur;
                                } else {
                                    $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                    $TITULAIRES_DENOMINATION = " ";
                                }
                                $service->insrerTitulaire($TITULAIRES_ID_TYPE, $TITULAIRES_ID, $TITULAIRES_DENOMINATION, $id);
                            }
                        }
                        if ($parentKey == $listeXml[19]) {//"modification:objetModification"
                            $test = true;
                            $estMarche = true;
                            $estTitulaire = false;
                            if ((isset($valeur)) && ($valeur != "")) {
                                $MODIF_OBJET = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MODIF_OBJET = " ";
                            }
                        }
                        if ($parentKey == $listeXml[20]) { //"modification:dateNotificationModification"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $MODIF_NOTIFICATION_DATE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MODIF_NOTIFICATION_DATE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[21]) { //"modification:datePublicationDonneesModification"
                            if ((isset($valeur)) && ($valeur != "")) {
                                $MODIF_PUBLICATION_DATE = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MODIF_PUBLICATION_DATE = " ";
                            }
                        }
                        if ($parentKey == $listeXml[22]) { //"modification:dureeMois"
                            if ((isset($value)) && ($valeur != "")) {
                                $MODIF_DUREE_MOIS = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MODIF_DUREE_MOIS = " ";
                            }
                        }
                        if ($parentKey == $listeXml[23]) {//"modification:montant"
                            if ((isset($value)) && ($valeur != "")) {
                                $MODIF_MARCHE_MONTANT = $valeur;
                            } else {
                                $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                $MODIF_MARCHE_MONTANT = " ";
                            }
                        }
                        if ($test) {
                            if ($estMarche) {
                                $idModif = $service->insererModification($MODIF_OBJET, $MODIF_PUBLICATION_DATE, $MODIF_NOTIFICATION_DATE, $DUREE_MOIS, $MONTANT, $MARCHE_ID);
                                $estMarche = false;
                            }
                            $obligatoir = $conf->modificationTitulaireFacultatifObligatoir($parentKey);
                            if ($parentKey == $listeXml[24]) { //"titulaire:typeIdentifiant"
                                //$obligatoir = $conf->obligatoirOuPas($parentKey);
                                if ((isset($valeur)) && ($valeur != "")) {
                                    $TITULAIRES_ID_TYPE = $valeur;
                                } else {
                                    $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                    $TITULAIRES_ID_TYPE = " ";
                                }
                            }
                            if ($parentKey == $listeXml[25]) { // "titulaire:id"
                                if ((isset($valeur)) && ($valeur != "")) {

                                    $TITULAIRES_ID = $valeur;
                                } else {
                                    $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                    $TITULAIRES_ID = " ";
                                }
                            }
                            if ($parentKey == $listeXml[26] ) { //"titulaire:denominationSociale"
                                if ((isset($valeur)) && ($valeur != "")) {

                                    $TITULAIRES_DENOMINATION = $valeur;
                                } else {
                                    $nbrchampsmanquent = $nbrchampsmanquent + $this->obligatoirOuPas($obligatoir);
                                    $TITULAIRES_DENOMINATION = " ";
                                }
                                $service->insererTitulaireModification($TITULAIRES_ID_TYPE, $TITULAIRES_ID, $TITULAIRES_DENOMINATION, $idModif);
                            }
                        }
                    } else {
                        $service->insererChampsFacultatif($champxml, $valeur, $MARCHE_ID);
                    }
                    $order++;
                }
                $estTitulaire = true;
                $test = false;
            }
            $service->insererlogErreur($nbrMarches,$nbrchampsmanquent,$nbrchmaps);
        }
        catch(\Exception $e)
        {
            var_dump($e->getMessage());
        }
        // j'ai deux informations le nombre de marche dans le fichier et le nombre des valeur null
        //echo $nbrchampsmanquent ."  ".$nbrchmaps;
        //$service->insererlogErreur($nbrchampsmanquent,$nbrchmaps);//faut que je renomme la migration plus jamias de majuscule dans les migration
    }

    /**
     * @param $liste  la liste qui contien tour les elements
     * @return array|false|int|string une liste des indice des idMarche
     */
    public function indiceIdMarche($liste)
    {
        $parentKey = 2;
        $indice = array();
        foreach ($liste as $lis) {
            $cle = $lis[$parentKey];
            if ($cle == "id") {
                $key = array_search($lis, $liste);
                //echo gettype($key)."<br>";
                $indice[] = $key;
            }
        }
        array_push($indice, count($liste));//la fin de la liste pour le decoupage
        return $indice;
    }

    /**
     * @param $liste la liste qui contien tout les elements
     * @return array une liste des marches
     */
    public function decouperListeMarches($liste)
    {
        $listeDecoupe = array();
        $indices = array();
        $indices = $this->indiceIdMarche($liste);

        for ($i = 0; $i < count($indices) - 1; $i++) {
            $var = $indices[$i + 1] - $indices[$i];
            $listeDecoupe[] = array_slice($liste, $indices[$i], $var);
        }
        return $listeDecoupe;
    }

    /**
     * la fonction me permet de recupperer un liste de liste
     * @param $obj l'objet qui contien des imbriquation
     * @param $parent l'element parent de l'objet
     * @param $indice l'indice du marche courant
     * @return array une liste avec plusieur information sur le chmaps
     */
    public function recparcour($obj, $parent)
    {
        //il ajoute une partie du marche suivant dans le marche courant !! faut que je creer une fonction que pour supprimer les doublants
        $indice = 0;
        foreach ($obj as $key => $value) {
            if (count($value)) {
                $this->recparcour($value, $key);
            } else {
                if ($key != "modifications") {
                    if ($parent == "null") {
                        $parentKey = $key;//$key
                    } else {
                        $parentKey = $parent . ":" . $key;
                    }
                    //$indice=$this->affectationCleConf($parentKey);
                    //xmlToCsv::$maliste[] = //,$indice,$parent
                    xmlToCsv::$maliste[] =array($key, $value, $parentKey);
                }
            }
        }
        return xmlToCsv::$maliste;
        //return xmlToCsv::$maliste;
    }

    /**
     * @param $fichier le fichier xml a lire
     * @return \DOMDocument|\SimpleXMLElement l'objet xml
     */
    public function lectureXml($fichier)
    {
        var_dump($fichier);
        try {
            $xml = new \DOMDocument();
            $xml = simplexml_load_file($fichier);
            return $xml;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * @param $fichier a enregiste dans le repertoir Storage avec le nom fichierXML
     *
     *
     * public function enregisterXml($fichier,$nomFichier)
     * {
     * $fichier->storeAs('public',$nomFichier);//self::$nomFichier
     * @return true si le fichier est du format xml
     * }*/

    public function urlXml($url)
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);//$request->input('url')
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);// true = sous forme de chaine;false sous forme de contenue CURLOPT_RETURNTRANSFER
            $contenu = curl_exec($curl);
            $doc = new \DOMDocument();
            $doc->loadXML($contenu);
            //dd($doc);
            $doc->save('nouveauFichier.xml');
            return true;

        } catch (\Exception $e) {
            return false;
        }
    }


    public function affectationIndice($listesDecoupe, $listeChampsXml)
    {
        //si ca ne marche pas quand il trouve un champs inconnu il ne sais pas quoi faire;
        $listindice = $this->listeIndice($listesDecoupe, $listeChampsXml);
        for ($i = 0; $i < count($listesDecoupe); $i++) {
            for ($j = 0; $j < count($listesDecoupe[$i]); $j++) {
                // faut que je teste sur l'existance dans l'xml
                $listesDecoupe[$i][$j][] = $listindice[$i][$j];
            }
        }
        return $listesDecoupe;
    }

    /**
     * @param $listesDecoupe la liste qui contien les donnees
     * @param $listeChampsXml la liste qui contien les champs xml
     * @return array avec un champs de plus dans la 1er liste c'est l'indice du champs xml
     */
    public function listeIndice($listesDecoupe, $listeChampsXml)
    {

        $parentcle = 2;
        $champsSurnumeraire = 9999;
        $listeComplaite = array();
        $listechamps = array("acheteur:id", "acheteur:nom", "titulaire:typeIdentifiant", "titulaire:id", "titulaire:denominationSociale");
        foreach ($listesDecoupe as $liste) {
            $indice = -1;
            $lis = array();
            for ($i = 0; $i < count($liste); $i++) {
                if ((in_array($liste[$i][$parentcle], $listechamps)) && (in_array($liste[$i][$parentcle], $listeChampsXml))) {
                    $cle = array_search($liste[$i][$parentcle], $listeChampsXml);
                    $indice = $cle;
                    $lis[$i] = $indice;
                }
                if ((!in_array($liste[$i][$parentcle], $listechamps)) && (in_array($liste[$i][$parentcle], $listeChampsXml))) {
                    $indice++;
                    $lis[$i] = $indice;
                }
                if ((!in_array($liste[$i][$parentcle], $listechamps)) && (!in_array($liste[$i][$parentcle], $listeChampsXml))) {
                    $indice++;
                    $lis[$i] = $champsSurnumeraire;
                }
            }
            $listeComplaite[] = $lis;
        }
        return $listeComplaite;
    }

    /**
     * @param $champs contien sois 1 sois 0 Sois NULL
     * @return sois 1 ou 0 selon la valeur du chmap passe en paramettre
     */
    public function obligatoirOuPas($champs)
    {
        return ($champs == 1) ? 1 : 0;
    }

    /**
     * @param $listeCourante marche public
     * @param $listeChampsXml liste qui valide le contenu du marche public
     * @param $listeObligatoir liste des champs obligatoir
     * @return true si l'ordre est correcte sinon il retourn false
     */
    public function traitementCasNormal($listeCourante, $listeChampsXml, $listeObligatoir)
    {
        try {
            //dd($listeChampsXml);
            $parentKey = 2;
            $obligatoir = 1;
            $cpt = 0;
            for ($i = 0; $i < count($listeCourante); $i++) {
                if ($listeObligatoir[$i] == $obligatoir) {
                    //var_dump($listeCourante[$i][$parentKey]."  ".$listeChampsXml[$i]);
                    if ($listeCourante[$i][$parentKey] != $listeChampsXml[$i]) {
                        return false;
                    }
                }
            }
            return true;

        } catch (\Exception $e) {
            return false;
        }

    }

    public function traitementCasImbrication($listeCourante, $listeChampsXml, $listeObligatoir)
    {
        try {
            $parentKey = 2;
            $nomchamps = 0;
            $indicechampsXml = 3;
            for ($i = 0; $i < count($listeChampsXml); $i++)//
            {
                $obligatoir = $listeObligatoir[$i];
                if (($obligatoir == 1 && ($listeCourante[$i][$indicechampsXml] != 9999))) {
                    $var = array_search($listeCourante[$i][2], $listeChampsXml);//
                    //echo $listeCourante[$i][0] . "  " . $listeCourante[$i][3] . "  " . $var;
                    if ($listeCourante[$i][3] !== $var) {
                        return false;
                    }
                }
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $lignes listes contenu du fichier CSV
     */
    function creerFichierCsv($lignes)
    {
        $fichier_csv = fopen("xmlToCsvFichier.csv", 'w+');
        foreach ($lignes as $ligne) {
            fputcsv($fichier_csv, $ligne, ",");
        }
        fclose($fichier_csv);
    }
}

class XmlToFormat
{
    use xmlToCsv;
}