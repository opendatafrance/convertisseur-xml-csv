<?php

namespace convertisseurXML\Http\Controllers;

use convertisseurXML\Http\Outils\XmlToFormat;
use Illuminate\Http\Request;

class ConvertisseurController extends Controller
{
    //
    function validateur()
    {
        return view('convertireForm');
    }

    function traitement(Request $request)
    {
        $params = $request->all();
        $xmltoformat = new XmlToFormat();

        try {
            if ((count($params) == 2) && (array_key_exists("fichier",$params))) {
                //$extFichier = $_FILES['fichier']['type'];
                //$nomFichier = $_FILES['fichier']['name'];
                if ($_FILES['fichier']['type'] == "text/xml") {
                    //$request->file('fichier')->storeAs('public', $_FILES['fichier']['name']);
                    //$xmltoformat->enregisterXml($request->file('fichier'),$nomFichier);
                    $etat=$xmltoformat->verificationChamps($request->file('fichier'));
                    if($etat!=null){

                        foreach ($etat as $key => $value)
                        {
                            $log = "le nombre de marchés est :".$value->nbrMarches." le nombre de champs manquants : ".$value->nbrChampsManquants." le nombre de champs total : ".$value->nbrLigne;
                        }
                        return redirect('validateur')->with("ok",$log);

                    }
                    else{
                        return redirect('validateur')->with("erreur","Fichier n'est pas conforme au SCDL");
                    }
                }
                else
                {
                    return redirect('validateur')->with("erreur","Fichier n'est pas un Xml");
                }
            }
            if((count($params) == 2) && (array_key_exists("url",$params))){
                $estXml = $xmltoformat->urlXml($params['url']);
                if($estXml)
                {
                    $etat=$xmltoformat->verificationChamps("nouveauFichier.xml");
                    if($etat!=null){
                        foreach ($etat as $key => $value)
                        {
                            $log = "le nombre de marchés est :".$value->nbrMarches." le nombre de champs manquants : ".$value->nbrChampsManquants." le nombre de champs total : ".$value->nbrLigne;
                        }
                        return redirect('validateur')->with("ok",$log);

                    }
                    else{
                        return redirect('validateur')->with("erreur","Fichier n'est pas conforme au schema reglementaire");
                    }
                }
                else{
                    return redirect('validateur')->with('erreur',"Lien n'est pas valide ");
                }
            }
        }
        catch( \Exception $e)
        {
            $e->getMessage();
        }
    }

    public function redirectionUtilisateur($etat){
        if($etat!=null){
            foreach ($etat as $key => $value)
            {
                $log = "le nombre de marchés est :".$value->nbrMarches." le nombre de champs manquants : ".$value->nbrChampsManquants." le nombre de champs total : ".$value->nbrLigne;
            }
            return redirect('validateur')->with("ok",$log);

        }
        else{
            return redirect('validateur')->with("erreur","Fichier n'est pas conforme au schema reglementaire");
        }
    }

}
