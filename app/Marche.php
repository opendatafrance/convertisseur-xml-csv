<?php

namespace convertisseurXML;

use Illuminate\Database\Eloquent\Model;

class Marche extends Model
{
    //
    protected $fillable =["id","idMarche","iud","nature","objet","codeCPV","procedure","code","typeCode","nom",
        "dureeMois","dateNotification","datePublicationDonnee","montant","formePrix"];
}
