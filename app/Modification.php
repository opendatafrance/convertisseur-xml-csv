<?php

namespace convertisseurXML;

use Illuminate\Database\Eloquent\Model;

class Modification extends Model
{
    //
    protected $fillable = ["id","objetModification","datePubDonneesModification","dateNotifModification","dureeMois","montant","idMarcheModification"];

}
