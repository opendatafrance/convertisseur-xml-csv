<?php

namespace convertisseurXML;

use Illuminate\Database\Eloquent\Model;

class Titulaire extends Model
{
    //
    protected $fillable = ["id","typeIdentifiant","denominationSociale","idMarche","idModification"];
}
