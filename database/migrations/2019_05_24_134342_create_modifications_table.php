<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modifications', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('objetModification')->nullable();
            $table->string('datePubDonneesModification')->nullable();
            $table->string('dateNotifModification')->nullable();
            $table->integer('dureeMois')->nullable();
            $table->double('montant')->nullable();
            $table->string('idMarcheModification')->nullable();

            $table->foreign('idMarcheModification')->references('id')->on('marches')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modifications');
    }
}
