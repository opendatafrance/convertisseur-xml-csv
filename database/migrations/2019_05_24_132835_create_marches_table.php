<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('idMarche')->nullable();
            $table->string('iud')->nullable();
            $table->string('nature')->nullable();
            $table->string('objet')->nullable();
            $table->string('codeCPV')->nullable();
            $table->string('procedure')->nullable();
            $table->integer('code')->nullable();
            $table->string('typeCode')->nullable();
            $table->string('nom')->nullable();
            $table->integer('dureeMois')->nullable();
            $table->string('dateNotification')->nullable();
            $table->string('datePublicationDonnee')->nullable();
            $table->double('montant')->nullable();
            $table->string('formePrix')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marches');
    }
}
