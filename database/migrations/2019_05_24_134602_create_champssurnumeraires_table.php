<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChampssurnumerairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champssurnumeraires', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('cleChamp')->nullable();
            $table->string('valeurChamp')->nullable();
            $table->integer('idMarche')->nullable();

            $table->foreign('idMarche')->references('id')->on('marches')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champssurnumeraires');
    }
}
