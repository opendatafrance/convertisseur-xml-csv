<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitulairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titulaires', function (Blueprint $table) {
            $table->integer('id');
            $table->timestamps();
            $table->string('typeIdentifiant')->nullable();
            $table->String('denominationSociale')->nullable();
            $table->integer('idMarche')->nullable();
            $table->integer('idModification')->nullable();

            $table->foreign('idModification')->references('id')->on('modification')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('idMarche')->references('id')->on('marches')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titulaires');
    }
}
