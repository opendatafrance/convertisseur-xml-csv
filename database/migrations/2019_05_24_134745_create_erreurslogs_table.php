<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErreurslogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erreurslogs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('nbrMarches')->nullable();
            $table->integer('nbrColonneDesordre')->nullable();
            $table->String('nbrChampsManquants')->nullable();
            $table->integer('nbrLigne')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erreurslogs');
    }
}
