## About Convertisseur-Xml-Csv

Au plus tard le 1er octobre 2018, les collectivités locales, doivent offrir sur leur profil d'acheteur, un accès libre, direct et complet aux données essentielles des marchés publics répondant à un besoin dont la valeur est égale ou supérieure à 25 000€ HT. La nature et les modalités de diffusion de ces données essentielles ont été fixées par voie réglementaire

les fichiers des marchés publics sont produits dans un format Xml qu'est incompiensive par les êtres humains.
Ce module permet de convertir ses fichiers en format tabulaire pour les rendre exploitables.


## liens complémentaires


* [Arrêté du 27 juillet 2018 modifiant l’arrêté du 14 avril 2017
relatif aux données essentielles dans la commande publique]( https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000037282994)
* [Arrêté du 14 avril 2017 relatif aux fonctionnalités
et exigences minimales des profils d’acheteurs ]( https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000034492557)

__Pour plus d’informations__ : [Marchés publics]( https://scdl.opendatafrance.net/docs/schemas/scdl-marches-publics.html)

## Fichiers test 

[lien](https://github.com/HamidiAyoub/Marches-publics/tree/master/March%C3%A9s-Public-Xml)


